<?php

use Illuminate\Database\Seeder;
use App\Marca;
use App\Categoria;
use App\Producto;

class ProductoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	  DB::table('productos')->delete();

        $today = date('Y-m-d H:i:s');

       $marc=Marca::all();
      $cat=Categoria::all(); 
         
         	$data =  [
       	
       	 [  'nombre' =>"Mantequilla Colun",
            'descripcion'=>"Mantequilla Colun 200gr",
            'precio'=>"2329",
            'codigo'=>"0030031",
            'imagen' =>"https://jumbo.vteximg.com.br/arquivos/ids/301976-250-250/Principal-10215.jpg?v=636931026363200000",
            'stock' => "3",
            'categoria_id' => $cat->id=(3),
            'marca_id'=>$marc->id=(3)
     	 ],

         [   
            'nombre' =>"Leche Colun",
            'descripcion'=>"Leche Colun",
            'precio'=>"859",
            'codigo'=>"0030032",
            'imagen' =>"https://www.supermercarro.cl/StoreSuper/339-large_default/leche-colun-entera-1l.jpg",
            'stock' => "2",
            'categoria_id' => $cat->id=(3),
            'marca_id'=>$marc->id=(3)
         ],

          [ 'nombre' =>"Manjar Colun",
            'descripcion'=>"Manjar Colun",
            'precio'=>"2879",
            'codigo'=>"0030033",
            'imagen' =>"http://www.colun.cl/resources/upload/74a23e0ececa07b8331f5c966d2ca5f9.png",
            'stock' => "5",
            'categoria_id' => $cat->id=(3),
            'marca_id'=>$marc->id=(3)
         ],

         [  'nombre' =>"Queso Gouda Laminado",
            'descripcion'=>"Queso Laminado",
            'precio'=>"3879",
            'codigo'=>"0050024",
            'imagen' =>"http://www.colun.cl/resources/upload/6d8810e1cd629dd6e6b565e79256a532.png",
            'stock' => '3',
            'categoria_id' => $cat->id=(5),
            'marca_id'=>$marc->id=(2)
         ],

          [ 'nombre' =>"Papas Duquesas",
            'descripcion'=>"Papas Duquesas Minuto Verde 1kg",
            'precio'=>"2490",
            'codigo'=>"0080165",
            'imagen' =>"https://jumbo.vteximg.com.br/arquivos/ids/160254-250-250/438964.jpg?v=636149844229100000",
            'stock' => '4',
            'categoria_id' => $cat->id=(8),
            'marca_id'=>$marc->id=(16)
         ],

          [ 'nombre' =>"Caja de Hamburguesas",
            'descripcion'=>"Vacuno 1kg 10 unidades",
            'precio'=>"5039",
            'codigo'=>"0080176",
            'imagen' =>"https://jumbo.vteximg.com.br/arquivos/ids/241344-250-250/467186.jpg?v=636639836125200000",
            'stock' => '5',
            'categoria_id' => $cat->id=(8),
            'marca_id'=>$marc->id=(17)
         ],
          [ 'nombre' =>"Choclo minuto verde",
            'descripcion'=>"Choclo Minuto Verde 500g",
            'precio'=>"1499",
            'codigo'=>"0080167",
            'imagen' =>"https://jumbo.vteximg.com.br/arquivos/ids/160357-250-250/255247.jpg?v=636149845129000000",
            'stock' => '3',
            'categoria_id' => $cat->id=(8),
            'marca_id'=>$marc->id=(16),
         ],
         [  'nombre' =>"Nuggets De Pollo La Crianza",
            'descripcion'=>"Nuggets De Pollo 400g",
            'precio'=>"1990",
            'codigo'=>"0080178",
            'imagen' =>"https://jumbo.vteximg.com.br/arquivos/ids/241354-250-250/928560.jpg?v=636639840042670000",
            'stock' => '5',
            'categoria_id' => $cat->id=(8),
            'marca_id'=>$marc->id=(17)
         ],

         [  'nombre' =>"Nescafé",
            'descripcion'=>"Café instantáneo Nescafé Tradicion 170g",
            'precio'=>'2799',
            'codigo'=>"0160199",
            'imagen' =>"https://jumbo.vteximg.com.br/arquivos/ids/293771-250-250/Principal-5171.jpg?v=636867996395800000",
            'stock' => '12',
            'categoria_id' => $cat->id=(16),
            'marca_id'=>$marc->id=(19)
         ],
        
         [  'nombre' =>"Nescafé",
            'descripcion'=>"Café Nescafé Foma Seleccion Frasco 170g",
            'precio'=>'3493',
            'codigo'=>"01601910",
            'imagen' =>"https://jumbo.vteximg.com.br/arquivos/ids/293765-250-250/Principal-5162.jpg?v=636867996351200000",
            'stock' => '12',
            'categoria_id' => $cat->id=(16),
            'marca_id'=>$marc->id=(19)
         ],

          [ 'nombre' =>"IANSA",
            'descripcion'=>"IANSA Blanca Granulada iansa 1kg",
            'precio'=>'769',
            'codigo'=>"01601811",
            'imagen' =>"https://jumbo.vteximg.com.br/arquivos/ids/293765-250-250/Principal-5162.jpg?v=636867996351200000",
            'stock' => '10',
            'categoria_id' => $cat->id=(16),
            'marca_id'=>$marc->id=(18)
         ],
          [ 'nombre' =>"Torta Cecilia",
            'descripcion'=>"15 Porciones",
            'precio'=>'14990',
            'codigo'=>"01701112",
            'imagen' =>"https://jumbo.vteximg.com.br/arquivos/ids/290699-250-250/Principal-11143.jpg?v=636855750435670000",
            'stock' => '1',
            'categoria_id' => $cat->id=(17),
            'marca_id'=>$marc->id=(11)
         ],
         [  'nombre' =>"Torta Cecilia Crema de Castañas",
            'descripcion'=>"15 Porciones",
            'precio'=>'14990',
            'codigo'=>"01701113",
            'imagen' =>"https://jumbo.vteximg.com.br/arquivos/ids/180721-250-250/1468581.jpg?v=636406388679170000",
            'stock' => '2',
            'categoria_id' => $cat->id=(17),
            'marca_id'=>$marc->id=(11)
         ],
         [  'nombre' =>"Posta Negra",
            'descripcion'=>"Desgrasado Envasado",
            'precio'=>'4794',
            'codigo'=>"01800114",
            'imagen' =>"https://jumbo.vteximg.com.br/arquivos/ids/238991-250-250/1661532-KG.jpg?v=636631928474870000",
            'stock' => '5',
            'categoria_id' => $cat->id=(18),
            'marca_id'=>$marc->id=(1)
         ],
         
          [ 'nombre' =>"Carne Molida",
            'descripcion'=>"Materia Grasa 500g",
            'precio'=>'3190',
            'codigo'=>"01800115",
            'imagen' =>"https://jumbo.vteximg.com.br/arquivos/ids/170308-250-250/261944.png?v=636159246181400000",
            'stock' => '5',
            'categoria_id' => $cat->id=(18),
            'marca_id'=>$marc->id=(1)
         ]
	
	];

	foreach ($data as $productos) {
            DB::table('productos')->insert([
                'nombre' => $productos['nombre'],
                'descripcion' => $productos['descripcion'],
                'precio' => $productos['precio'],
                'codigo' => $productos['codigo'],
                'imagen' => $productos['imagen'],
                'stock' => $productos['stock'],
                'categoria_id' => $productos['categoria_id'],
                'marca_id' => $productos['marca_id'],
                'created_at' => $today,
                'updated_at' => $today
            ]);
        }


    }
}
