<?php

use Illuminate\Database\Seeder;

class CategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
            DB::table('categorias')->delete();

        $today = date('Y-m-d H:i:s');

        $data = [
           
            ['nombre' => 'Frutas y Verduras',
              'descripcion'=>'',
              'codigo'=>'001'
            ],
            ['nombre' => 'Panaderia y Dulces',
              'descripcion'=>'',
               'codigo'=>'002'
            ],
            ['nombre' => 'Lácteos y Bebidas Vegetales',
              'descripcion'=>'',
               'codigo'=>'003'
            ],
            ['nombre' => 'Despensa',
               'descripcion'=>'',
                'codigo'=>'004'
            ],
            ['nombre' => 'Quesos y Fiambres',
              'descripcion'=>'',
               'codigo'=>'005'
            ],
            ['nombre' => 'Pollo y Pavo',
               'descripcion'=>'',
                'codigo'=>'006'  
            ],
            ['nombre' => 'Pescadería',
               'descripcion'=>'',
                'codigo'=>'007'
            ],
            ['nombre' => 'Congelados',
               'descripcion'=>'' ,
                'codigo'=>'008'
            ],
            ['nombre' => 'Panadería y Pasteleria',
               'descripcion'=>'',
                'codigo'=>'009'  
            ],
            ['nombre' => 'Platos Preparados y Pastas',
              'descripcion'=>'',
               'codigo'=>'010'
            ],
            ['nombre' => 'Bebidas,Aguas y Jugos',
               'descripcion'=>'',
                'codigo'=>'011'
            ],
            ['nombre' => 'Vinos,Cervezas y Licores',
               'descripcion'=>'',
                'codigo'=>'012'
            ],
            ['nombre' => 'Farmacia',
              'descripcion'=>'',
               'codigo'=>'013'
            ],
            ['nombre' => 'Perfumeria',
               'descripcion'=>'',
                'codigo'=>'014'
            ],
            ['nombre' => 'Limpieza',
               'descripcion'=>'',
                'codigo'=>'015'  
            ],
            ['nombre' => 'Desayuno y Dulces',
               'descripcion'=>'',
                'codigo'=>'016' 
            ],
             ['nombre' => 'Tortas',
               'descripcion'=>'',
                'codigo'=>'017'  
            ],
             ['nombre' => 'Carnes',
               'descripcion'=>'' ,
                'codigo'=>'018'
            ]
        ];

        foreach ($data as $categorias) {
            DB::table('categorias')->insert([
                'nombre' => $categorias['nombre'],
                'descripcion' => $categorias['descripcion'],
                'codigo' => $categorias['codigo'],
                'created_at' => $today,
                'updated_at' => $today
            ]);



        }

    }
}
