<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $this->call(CiudadTableSeeder::class);
       $this->call(CategoriaTableSeeder::class);
       $this->call(MarcaTableSeeder::class);
       $this->call(ProveedorTableSeeder::class);
       $this->call(ProductoTableSeeder::class);
       $this->call(UserTableSeeder::class);
       
    }
}
