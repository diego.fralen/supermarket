<?php

use Illuminate\Database\Seeder;

class MarcaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('marcas')->delete();

        $today = date('Y-m-d H:i:s');

        $data = [
            ['nombre' => 'Agrosuper','codigo'=>'001'],
            ['nombre' => 'Gouda','codigo'=>'002'],
            ['nombre' => 'Colun','codigo'=>'003'],
            ['nombre' => 'Quillayes','codigo'=>'004'],
            ['nombre' => 'Carozzi','codigo'=>'005'],
            ['nombre' => 'Lucchetti','codigo'=>'006'],
            ['nombre' => 'Coca-Cola','codigo'=>'007'],
            ['nombre' => 'Pepsi','codigo'=>'008'],
            ['nombre' => 'Sprite','codigo'=>'009'],
            ['nombre' => 'Calo','codigo'=>'010'],
            ['nombre' => 'Nestle','codigo'=>'011'],
            ['nombre' => 'Soprole','codigo'=>'012'],
            ['nombre' => 'Philadelphia','codigo'=>'013'],
            ['nombre' => 'Rio Bueno','codigo'=>'014'],
            ['nombre' => 'SuperCerdo','codigo'=>'015'],
            ['nombre' => 'Minuto Verde','codigo'=>'016'],
            ['nombre' => 'La Crianza','codigo'=>'017'],
            ['nombre' => 'IANSA','codigo'=>'018'],
            ['nombre' => 'Nescafé','codigo'=>'019']
        ];

        foreach ($data as $marcas) {
            DB::table('marcas')->insert([
                'nombre' => $marcas['nombre'],
                'codigo' =>$marcas['codigo'],
                'created_at' => $today,
                'updated_at' => $today
            ]);



        }
    }
}
