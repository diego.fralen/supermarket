<?php

use Illuminate\Database\Seeder;
use App\Proveedor;

class ProveedorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	$proveedor =  [
       	 [
            'nombre' =>"Diego",
            'apellido'=>"ramos",
            'direccion'=>"pasaje buenos aires",
            'rut'=>"19.151.646-k",
            'telefono'=>"958518229",
            'email' =>"diegoestudiante34@hotmail.com",
     	 ],
	];

	 for ($i=0; $i < count($proveedor) ; $i++) {

          Proveedor::create($proveedor[$i]);
        };

    }
}
