<?php

use Illuminate\Database\Seeder;
use App\Ciudad;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        	$ciudads_creados=Ciudad::all(); 
         
         	$usuarios =  [
       	 [	'rut'=>"19.151.646-k",
            'nombre' =>"Diego",
            'apellido'=>"ramos",
            'telefono'=>"958518229",
            'direccion'=>"pasaje buenos aires",
            'email' =>"diegoestudiante34@hotmail.com",
            'password' => bcrypt('123123'),
            'ciudad_id' => $ciudads_creados->random()->id
     	 ],
	];

	 for ($i=0; $i < count($usuarios) ; $i++) {

          User::create($usuarios[$i]);
        };

    }
}
