<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_facturas', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->string('numero_factura');
            $table->date('fecha_compra');
            $table->integer('cantidad');
            $table->float('precio_venta');
            $table->float('total');
            $table->unsignedBigInteger('proveedor_id');
           $table->foreign('proveedor_id')->references('id')->on('proveedores');
             $table->unsignedBigInteger('producto_id');
           $table->foreign('producto_id')->references('id')->on('productos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_facturas');
    }
}
