<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewDetalleVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       DB::statement("CREATE VIEW detalleventas AS SELECT  'users.rut' , 'CONCAT(users.nombre," ",users.apellido) as cliente', 'users.telefono','productos.nombre AS Producto', 'productos.imagen', 'productos.codigo', 'producto_ventas.id AS Numero_Boleta', 'producto_ventas.fecha_venta', 'ventas.cantidad AS Cantidad','productos.precio*ventas.cantidad AS precio_productos', 'producto_ventas.total AS Total_Boleta'
        AS value FROM productos;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_detalle_ventas');
    }
}
