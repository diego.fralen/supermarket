<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('descripcion');
            $table->float('precio');
            $table->string('codigo');
             $table->string('imagen');
             $table->integer('stock');
            $table->timestamps();
            $table->unsignedBigInteger('categoria_id');
           $table->foreign('categoria_id')->references('id')->on('categorias');
           $table->unsignedBigInteger('marca_id');
           $table->foreign('marca_id')->references('id')->on('marcas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
