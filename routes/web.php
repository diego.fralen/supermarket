<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//


Route::get('/','HomeController@show');

Auth::routes();

Route::get('/menu','UsuarioController@index');
Route::get('/checkout','UsuarioController@checkout');


Route::prefix('admin')->group(function () {
    //inicio admin
    Route::get('/', 'AdminController@index')->name('admin.dashboard');

    //formulario admin loguin
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');

    //este se supone que redirige al admin
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

    Route::group(['middleware' => 'auth:api'], function() {
  Route::get('/logout', 'UsuarioController@logout');
});
});

