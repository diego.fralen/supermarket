<?php

use Illuminate\Http\Request;
use App\Categoria;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Mostrar productos
Route::get('productos', 'Api@listas_producto');
//CREAR PRODUCTOS
Route::post('rproductos','Api@createproductos');
//Eliminar Productos
 Route::delete('deleteprod/{id}','Api@deleteprod');
//Editar Productos
 Route::get('editprod/{id}','Api@editprod');
 //ACtualizar PRODUCTOS
   Route::put('updateprod/{id}','Api@updateprod');

//Mostrar Categorias
Route::get('categorias', 'Api@listas_categoria');
Route::get('categoriapro','Api@categoriaproducto');
//AGREGAR CATEEGORIAS
Route::post('rcategoria','Api@create');
//Eliminar Categorias
 Route::delete('deletecat/{id}','Api@destroy');
//EDITAR CATEGORIAS
 Route::get('editar/{id}','Api@edit');
  Route::put('update/{id}','Api@update');


//Mostrar Marcas
  Route::get('marcas','Api@marcas');


//Mostrar Proveedores
Route::get('proveedores', 'Api@listas_proveedor');
//Crear Proveedores
Route::post('rproveedores','Api@createpro');
//ELIMINAR PROVEEDORES
 Route::delete('deleteprov/{id}','Api@deleteprov');

//MOSTRAR CLIENTES EN ADMIN
Route::get('showuser','Api@showuser');
//ELIMINAR CLIENTES EN ADMIN
 Route::delete('deletecli/{id}','Api@removecli'); 


 //MOSTRAR COMPRA_PRODUCTOS DE PROVEEDORES
Route::get('showdetalles','Api@showcompra_prov');
Route::post('compra_prov','Api@compra_prov');

//Eliminar factura
Route::delete('deletefactura/{id}','Api@deletefactura');

//COMPRA DE CLIENTES
Route::post('producto_ventas','Api@compra');
Route::get('carrito','Api@muestratectm');

Route::get('detalleventas','Api@detalleventas');