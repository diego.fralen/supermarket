-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.37-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para shoppingdos
CREATE DATABASE IF NOT EXISTS `shoppingdos` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `shoppingdos`;

-- Volcando estructura para tabla shoppingdos.admins
CREATE TABLE IF NOT EXISTS `admins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido paterno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido materno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla shoppingdos.admins: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` (`id`, `nombre`, `apellido paterno`, `apellido materno`, `email`, `email_verified_at`, `created_at`, `updated_at`, `password`) VALUES
	(1, 'diego', 'ramos', 'rios', '', NULL, NULL, NULL, 'NAiX8OPvIdQCjHlByl8s8MHbI3+hTrYdMcCQyCZV/cU');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla shoppingdos.categorias: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.ciudad
CREATE TABLE IF NOT EXISTS `ciudad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=346 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla shoppingdos.ciudad: ~345 rows (aproximadamente)
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` (`id`, `nombre`) VALUES
	(1, 'Algarrobo'),
	(2, 'Alhue'),
	(3, 'Alto BioBío'),
	(4, 'Alto del Carmen'),
	(5, 'Alto Hospicio'),
	(6, 'Ancud'),
	(7, 'Andacollo'),
	(8, 'Angol'),
	(9, 'Antofagasta'),
	(10, 'Antuco'),
	(11, 'Arauco'),
	(12, 'Arica'),
	(13, 'Buin'),
	(14, 'Bulnes'),
	(15, 'Cabildo'),
	(16, 'Cabo de Hornos'),
	(17, 'Cabrero'),
	(18, 'Calama'),
	(19, 'Antártica'),
	(20, 'Calbuco'),
	(21, 'Caldera'),
	(22, 'Calera de Tango'),
	(23, 'Calle Larga'),
	(24, 'Camarones'),
	(25, 'Camiña'),
	(26, 'Canela'),
	(27, 'Cañete'),
	(28, 'Carahue'),
	(29, 'Carnameena'),
	(30, 'Casablanca'),
	(31, 'Castro'),
	(32, 'Catemu'),
	(33, 'Cauquenes'),
	(34, 'Cerrillos'),
	(35, 'Cerro Navia'),
	(36, 'Chaiten'),
	(37, 'Chañaral'),
	(38, 'Chanco'),
	(39, 'Chépica'),
	(40, 'Chiguayante'),
	(41, 'Chile Chico'),
	(42, 'Chillan'),
	(43, 'Chillan Viejo'),
	(44, 'Chimbarongo'),
	(45, 'Cholchol'),
	(46, 'Chonchi'),
	(47, 'Cobquecura'),
	(48, 'Cochamó'),
	(49, 'Cochrane'),
	(50, 'Codegua'),
	(51, 'Coelemu'),
	(52, 'Coihueco'),
	(53, 'Coinco'),
	(54, 'Colbun'),
	(55, 'Colchane'),
	(56, 'Colina'),
	(57, 'Collipulli'),
	(58, 'Coltauco'),
	(59, 'Combarbalá'),
	(60, 'Concepcion'),
	(61, 'Conchali'),
	(62, 'Concón'),
	(63, 'Constitucion'),
	(64, 'Contulmo'),
	(65, 'Copiapó'),
	(66, 'Coquimbo'),
	(67, 'Coronel'),
	(68, 'Coyhaique'),
	(69, 'Cunco'),
	(70, 'Curacautin'),
	(71, 'Curacavi'),
	(72, 'Curaco de Velez'),
	(73, 'Curanilahue'),
	(74, 'Curarrehue'),
	(75, 'Curepto'),
	(76, 'Curico'),
	(77, 'Dalcahue'),
	(78, 'Diego de Almagro'),
	(79, 'Doñihue'),
	(80, 'El Bosque'),
	(81, 'El Carmen'),
	(82, 'El Monte'),
	(83, 'El Quisco'),
	(84, 'El Tabo'),
	(85, 'Empedrado'),
	(86, 'Ercilla'),
	(87, 'Estacion Central'),
	(88, 'Florida'),
	(89, 'Freire'),
	(90, 'Freirina'),
	(91, 'Fresia'),
	(92, 'Frutillar'),
	(93, 'Futaleufu'),
	(94, 'Galvarino'),
	(95, 'General Lagos'),
	(96, 'Gorbea'),
	(97, 'Graneros'),
	(98, 'Guaitecas'),
	(99, 'Hijuelas'),
	(100, 'Hualaihue'),
	(101, 'Puerto Cisnes'),
	(102, 'Hualañé'),
	(103, 'Hualpén'),
	(104, 'Hualqui'),
	(105, 'Huara'),
	(106, 'Huasco'),
	(107, 'Huechuraba'),
	(108, 'Río Hurtado'),
	(109, 'Illapel'),
	(110, 'Independencia'),
	(111, 'Iquique'),
	(112, 'Isla de Maipo'),
	(113, 'Isla de Pascua'),
	(114, 'Juan Fernandez'),
	(115, 'La Calera'),
	(116, 'La Cisterna'),
	(117, 'La Cruz'),
	(118, 'La Estrella'),
	(119, 'La Florida'),
	(120, 'La Granja'),
	(121, 'La Higuera'),
	(122, 'La Laja'),
	(123, 'La Ligua'),
	(124, 'La Pintana'),
	(125, 'La Reina'),
	(126, 'La Serena'),
	(127, 'Lago Verde'),
	(128, 'Laguna Blanca'),
	(129, 'Lampa'),
	(130, 'Las Cabras'),
	(131, 'Las Condes'),
	(132, 'Puerto Aysen'),
	(133, 'Lautaro'),
	(134, 'Lebu'),
	(135, 'Licanten'),
	(136, 'Limache'),
	(137, 'Linares'),
	(138, 'Litueche'),
	(139, 'Llanquihue'),
	(140, 'Lo Barnechea'),
	(141, 'Lo Espejo'),
	(142, 'Lo Prado'),
	(143, 'Lolol'),
	(144, 'Loncoche'),
	(145, 'Longavi'),
	(146, 'Lonquimay'),
	(147, 'Los Alamos'),
	(148, 'Los Andes'),
	(149, 'Los Angeles'),
	(150, 'Los Muermos'),
	(151, 'Los Sauces'),
	(152, 'Los Vilos'),
	(153, 'Lota'),
	(154, 'Lumaco'),
	(155, 'Machalí'),
	(156, 'Macul'),
	(157, 'Maipu'),
	(158, 'Malloa'),
	(159, 'Marchihue'),
	(160, 'Maria Elena'),
	(161, 'Maria Pinto'),
	(162, 'Maule'),
	(163, 'Maullin'),
	(164, 'Mejillones'),
	(165, 'Melipeuco'),
	(166, 'Melipilla'),
	(167, 'Molina'),
	(168, 'Monte Patria'),
	(169, 'Mulchen'),
	(170, 'Nacimiento'),
	(171, 'Nancahua'),
	(172, 'Navidad'),
	(173, 'Negrete'),
	(174, 'Ninhue'),
	(175, 'Ñiquén'),
	(176, 'Nogales'),
	(177, 'Nueva Imperial'),
	(178, 'Ñuñoa'),
	(179, 'O´Higgins'),
	(180, 'Olivar'),
	(181, 'Ollagüe'),
	(182, 'Olmue'),
	(183, 'Osorno'),
	(184, 'Ovalle'),
	(185, 'Padre Hurtado'),
	(186, 'Padre Las Casas'),
	(187, 'Paiguano'),
	(188, 'Paine'),
	(189, 'Palena'),
	(190, 'Palmilla'),
	(191, 'Panquehue'),
	(192, 'Papudo'),
	(193, 'Paredones'),
	(194, 'Parral'),
	(195, 'Pedro Aguirre Cerda'),
	(196, 'Pelarco'),
	(197, 'Pelluhue'),
	(198, 'Pemuco'),
	(199, 'Peñaflor'),
	(200, 'Peñalolen'),
	(201, 'Pencahue'),
	(202, 'Penco'),
	(203, 'Peralillo'),
	(204, 'Perquenco'),
	(205, 'Petorca'),
	(206, 'Peumo'),
	(207, 'Pica'),
	(208, 'Pichidegua'),
	(209, 'Pichilemu'),
	(210, 'Pinto'),
	(211, 'Pirque'),
	(212, 'Pitrufquen'),
	(213, 'Placilla'),
	(214, 'Portezuelo'),
	(215, 'Porvenir'),
	(216, 'Pozo Almonte'),
	(217, 'Primavera'),
	(218, 'Providencia'),
	(219, 'Puchuncaví'),
	(220, 'Pucón'),
	(221, 'Puerto Saavedra'),
	(222, 'Pudahuel'),
	(223, 'Puente Alto'),
	(224, 'Puerto Montt'),
	(225, 'Natales'),
	(226, 'Puerto Octay'),
	(227, 'Puerto Varas'),
	(228, 'Pumanque'),
	(229, 'Punitaqui'),
	(230, 'Punta Arenas'),
	(231, 'Puqueldon'),
	(232, 'Puren'),
	(233, 'Purranque'),
	(234, 'Putaendo'),
	(235, 'Putre'),
	(236, 'Puyehue'),
	(237, 'Queilen'),
	(238, 'Quellon (Puerto Quellon)'),
	(239, 'Quemchi'),
	(240, 'Quilaco'),
	(241, 'Quilicura'),
	(242, 'Quilleco'),
	(243, 'Quillon'),
	(244, 'Quillota'),
	(245, 'Quilpué'),
	(246, 'Tucapel'),
	(247, 'Quinchao'),
	(248, 'Quinta de Tilcoco'),
	(249, 'Quinta Normal'),
	(250, 'Quintero'),
	(251, 'Quirihue'),
	(252, 'Rancagua'),
	(253, 'Ranquil'),
	(254, 'Rapel'),
	(255, 'Rauco'),
	(256, 'Recoleta'),
	(257, 'Renaico'),
	(258, 'Renca'),
	(259, 'Rengo'),
	(260, 'Requinoa'),
	(261, 'Retiro'),
	(262, 'Rinconada de Los Andes'),
	(263, 'Rio Claro'),
	(264, 'Rio Ibanez'),
	(265, 'Rio Negro'),
	(266, 'Rio Verde'),
	(267, 'Romeral'),
	(268, 'Sagrada Familia'),
	(269, 'Salamanca'),
	(270, 'San Antonio'),
	(271, 'San Bernardo'),
	(272, 'San Carlos'),
	(273, 'San Clemente'),
	(274, 'San Esteban'),
	(275, 'San Fabian'),
	(276, 'San Felipe'),
	(277, 'San Fernando'),
	(278, 'San Gregorio'),
	(279, 'San Ignacio'),
	(280, 'San Javier'),
	(281, 'San Joaquin'),
	(282, 'San Jose de Maipo'),
	(283, 'San Juan de la Costa'),
	(284, 'San Miguel'),
	(285, 'San Nicolas'),
	(286, 'San Pablo'),
	(287, 'San Pedro'),
	(288, 'San Pedro de Atacama'),
	(289, 'San Pedro de la Paz'),
	(290, 'San Rafael'),
	(291, 'San Ramon'),
	(292, 'San Rosendo'),
	(293, 'San Vicente de Tagua Tagua'),
	(294, 'Santa Barbara'),
	(295, 'Santa Cruz'),
	(296, 'Santa Juana'),
	(297, 'Santa Maria'),
	(298, 'Santiago'),
	(299, 'Santo Domingo'),
	(300, 'Sierra Gorda'),
	(301, 'Talagante'),
	(302, 'Talca'),
	(303, 'Talcahuano'),
	(304, 'Taltal'),
	(305, 'Temuco'),
	(306, 'Teno'),
	(307, 'Teodoro Schmidt'),
	(308, 'Tierra Amarilla'),
	(309, 'Til til'),
	(310, 'Timaukel'),
	(311, 'Tirúa'),
	(312, 'Tocopilla'),
	(313, 'Tolten'),
	(314, 'Tome'),
	(315, 'Torres del Paine'),
	(316, 'Tortel'),
	(317, 'Traiguen'),
	(318, 'Treguaco'),
	(319, 'Vallenar'),
	(320, 'Valparaiso'),
	(321, 'Vichuquen'),
	(322, 'Victoria'),
	(323, 'Vicuña'),
	(324, 'Vilcun'),
	(325, 'Villa Alegre'),
	(326, 'Villa Alemana'),
	(327, 'Villarrica'),
	(328, 'Vina del Mar'),
	(329, 'Vitacura'),
	(330, 'Yerbas Buenas'),
	(331, 'Yumbel'),
	(332, 'Yungay'),
	(333, 'Zapallar'),
	(334, 'Corral'),
	(335, 'Lanco'),
	(336, 'Los Lagos'),
	(337, 'Mafil'),
	(338, 'Mariquina'),
	(339, 'Paillaco'),
	(340, 'Panguipulli'),
	(341, 'Valdivia'),
	(342, 'Futrono'),
	(343, 'La Unión'),
	(344, 'Lago Ranco'),
	(345, 'Rio Bueno');
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.descuentos
CREATE TABLE IF NOT EXISTS `descuentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `porcentaje` tinyint(4) DEFAULT NULL,
  `cantidad_producto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla shoppingdos.descuentos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `descuentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `descuentos` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.descuento_producto
CREATE TABLE IF NOT EXISTS `descuento_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descuentos_iddescuentos` int(11) NOT NULL,
  `productos_id` bigint(20) unsigned NOT NULL,
  `inicio` timestamp NULL DEFAULT NULL,
  `termino` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_descuento_producto_descuentos1_idx` (`descuentos_iddescuentos`),
  KEY `fk_descuento_producto_productos1_idx` (`productos_id`),
  CONSTRAINT `fk_descuento_producto_descuentos1` FOREIGN KEY (`descuentos_iddescuentos`) REFERENCES `descuentos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_descuento_producto_productos1` FOREIGN KEY (`productos_id`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla shoppingdos.descuento_producto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `descuento_producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `descuento_producto` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.detalle_facturas
CREATE TABLE IF NOT EXISTS `detalle_facturas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `precio` double NOT NULL,
  `cantidad` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `proveedor_id` bigint(20) unsigned NOT NULL,
  `productos_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `detalle_facturas_proveedor_id_foreign` (`proveedor_id`),
  KEY `fk_detalle_facturas_productos1_idx` (`productos_id`),
  CONSTRAINT `detalle_facturas_proveedor_id_foreign` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedores` (`id`),
  CONSTRAINT `fk_detalle_facturas_productos1` FOREIGN KEY (`productos_id`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla shoppingdos.detalle_facturas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `detalle_facturas` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_facturas` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.marcas
CREATE TABLE IF NOT EXISTS `marcas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla shoppingdos.marcas: ~19 rows (aproximadamente)
/*!40000 ALTER TABLE `marcas` DISABLE KEYS */;
INSERT INTO `marcas` (`id`, `nombre`) VALUES
	(1, 'agrosuper'),
	(2, 'colun'),
	(3, 'calo'),
	(4, 'SuperCerdo'),
	(5, 'quillayes'),
	(6, 'Coca-Cola'),
	(7, 'Acuenta'),
	(8, 'Carozzi'),
	(9, 'Lider'),
	(10, 'lucchetti'),
	(11, 'ElVive'),
	(12, 'Pantene'),
	(13, 'Head&Soulders'),
	(14, 'Sedal'),
	(15, 'Fructis'),
	(16, 'Dove'),
	(17, 'Gouda'),
	(18, 'Nestle'),
	(19, 'Nido');
/*!40000 ALTER TABLE `marcas` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla shoppingdos.migrations: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla shoppingdos.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.productos
CREATE TABLE IF NOT EXISTS `productos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `precio` double NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `categoria_id` bigint(20) unsigned NOT NULL,
  `stock_id` int(11) NOT NULL,
  `marca_id` int(11) NOT NULL,
  `codigo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `productos_categorias_id_foreign` (`categoria_id`),
  KEY `fk_productos_stock1_idx` (`stock_id`),
  KEY `fk_productos_marcas1_idx` (`marca_id`),
  CONSTRAINT `fk_productos_marcas1` FOREIGN KEY (`marca_id`) REFERENCES `marcas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_productos_stock1` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `productos_categorias_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla shoppingdos.productos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.producto_ventas
CREATE TABLE IF NOT EXISTS `producto_ventas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cantidad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `producto_id` bigint(20) unsigned NOT NULL,
  `devolucion_id` bigint(20) unsigned NOT NULL,
  `venta_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `producto_ventas_producto_id_foreign` (`producto_id`),
  KEY `producto_ventas_venta_id_foreign` (`venta_id`),
  CONSTRAINT `producto_ventas_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`),
  CONSTRAINT `producto_ventas_venta_id_foreign` FOREIGN KEY (`venta_id`) REFERENCES `ventas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla shoppingdos.producto_ventas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `producto_ventas` DISABLE KEYS */;
/*!40000 ALTER TABLE `producto_ventas` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.proveedores
CREATE TABLE IF NOT EXISTS `proveedores` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stock_id` int(11) NOT NULL,
  `rut` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proveedors_stock1_idx` (`stock_id`),
  CONSTRAINT `fk_proveedors_stock1` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla shoppingdos.proveedores: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.stock
CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla shoppingdos.stock: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
INSERT INTO `stock` (`id`, `cantidad`) VALUES
	(1, 0);
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rut` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comuna_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_rut_unique` (`rut`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `fk_users_comunas1_idx` (`comuna_id`),
  CONSTRAINT `fk_users_comunas1` FOREIGN KEY (`comuna_id`) REFERENCES `ciudad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla shoppingdos.users: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Volcando estructura para tabla shoppingdos.ventas
CREATE TABLE IF NOT EXISTS `ventas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ventas_user_id_foreign` (`user_id`),
  CONSTRAINT `ventas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla shoppingdos.ventas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
