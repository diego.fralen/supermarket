
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import VueNumberInput from '@chenfengyuan/vue-number-input';
import VueBarcode from 'vue-barcode';
import Paginate from 'vuejs-paginate';
window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

import producto from './components/productos'
import cart from './components/cart'
import checkout from './components/checkout'
Vue.component('menu-main', require('./components/menu.vue').default);
Vue.component('contenedor', require('./components/contenedor.vue').default);
Vue.component('contacto', require('./components/contacto.vue').default);
Vue.component('producto', require('./components/productos.vue').default);
Vue.component('cart', require('./components/cart.vue').default);
Vue.component('checkout', require('./components/checkout.vue').default);
Vue.component('number-input', VueNumberInput);
Vue.component('barcode',VueBarcode);
Vue.component('paginate', Paginate)
Vue.component('menu-admin' ,require('./components/Admin/admin.vue').default);
Vue.component('admin-categoria' ,require('./components/Admin/categorias.vue').default);
Vue.component('admin-proveedor' ,require('./components/Admin/proveedores.vue').default);
Vue.component('admin-producto' ,require('./components/Admin/productosadmin.vue').default);
Vue.component('admin-clientes' ,require('./components/Admin/clientes.vue').default);
Vue.component('admin-compraprov' ,require('./components/Admin/compraproveedor.vue').default);
Vue.component('admin-detalleventas' ,require('./components/Admin/detalleventas.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    components:{producto,cart},
    data:{carrito:[],
    	  total:0
    },
   
});
