<!DOCTYPE html>
<html>
<head>
    <title>SuperMarket</title>
   @include('partials.headers')

</head>
<body>

<!--LLAMANDO A LOS COMPONENTES VUE CON APP-->
<div id="app">



<!--PRIMER NAVBAR-->
<nav class="navbare navbar navbar-expand-md navbar-dark bg-dark flex-row col-xs-8">
        <a class="navbar-brand mr-auto" href="#">
            
            <h2 class="hdos">SuperMarket</h2>

              <form id="form" class="form-inline my-2 my-lg-0">
                <input id="control" class="form-control mr-sm-2" type="text" placeholder="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type=""><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>

                
          
        </a>
        
        <ul  class="navbar-nav flex-row mr-lg-5">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle mr-3 mr-lg-0" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-3x"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">

                    <!--MODAL-->
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#loginModal">login</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#registerModal">Registrate</a>
                </div>

                 <li class="nav-item">
                    
                
                
            </li>
            </li>
        </ul>
       <!-- <button class="navbar-toggler ml-lg-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>-->
    </nav>
    

    <!--SEGUNDO NAVBAR CON CATEGORIAS Y SUBCATEGORIAS-->

    <nav class="navbars navbar-expand-md navbar-light bg-light ">
        <div id="menu">
<ul>
<li><a href="#">SuperMarket</a>
<!-- start menu desplegable -->
 <ul class="header-links-menu-container-subcategories">
 <li class="listeilor"><a class="" href="#">Lacteos y Bebidas Vegetales </a>

<ul class="header-links-menu-categories-number-two">
    <li><a href="">Leches Blancas</a></li>
    <li><a href="">Leches Cultivadas</a></li>
    <li><a href="">Leches Saborizadas</a></li>
    <li><a href="">Bebidas Vegetales</a></li>
    <li><a href="">Yoghurt</a></li>
    <li><a href="">Postres</a></li>
    <li><a href="">Mantequillas y Margarinas</a></li>
    <li><a href="">Cremas</a></li>
    <li><a href="">Manjar y Dulce de Leche</a></li>
    <li><a href="">Huevos</a></li>
    <li><a href="">Leche en Polvo</a></li>
    <img class="imgmenu" src="https://jumbo.vteximg.com.br/arquivos/ids/174581/bg-lacteos2-min.jpg">
</ul>

 </li>
 <li class="listeilor"><a class="" href="#">Frutas y Verduras</a></li>
 <li class="listeilor"><a class="" href="#">Despensa</a></li>
 <li class="listeilor"><a class="" href="#">Carnes Rojas</a></li>
 <li class="listeilor"><a class="" href="#">Quesos y Fiambres</a></li>
 <li class="listeilor"><a class="" href="#">Pollo y Pavo</a></li>
 <li class="listeilor"><a class="" href="#">Pescaderia</a></li>
 <li class="listeilor"><a class="" href="#">Congelados</a></li>
 <li class="listeilor"><a class="" href="#">Panaderia y Pasteleria</a></li>
 <li class="listeilor"><a class="" href="#">Platos Preparados y Pastas</a></li>
 <li class="listeilor"><a class="" href="#">Bebidas,Aguas y Jugos</a></li>
 <li class="listeilor"><a class="" href="#">Vinos,Cervezas y Licores</a></li>
 <li class="listeilor"><a class="" href="#">Farmacia</a></li>
 <li class="listeilor"><a class="" href="#">Perfumeria</a></li>
 <li class="listeilor"><a class="" href="#">Limpieza</a></li>
 <li class="listeilor"><a class="" href="#">Desayuno y Dulces</a></li>
 </ul>

</li>
<li><a href="#">Ofertas</a></li>
<li><a href="#">Tecnologia</a></li>
<li><a href="#">Hogar</a></li>
<li><a href="#">Mascotas</a></li>
<li><a href="#">deportes</a></li>
<li><a href="#">Librerias</a></li>
<li><a href="#">Jugueteria</a></li>
<li><a href="#">Mundo Bebe</a></li>
</ul>
</div>
    </nav>

<!--AQUI LLAMO EL CAROUSEL CON SUS RESPECTIVAS FOTOS-->

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="https://www.jumbo.cl/uploads/2019/04/BANNER-DESKTOP.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://www.jumbo.cl/uploads/2019/04/Home-desktop_vodka.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://www.jumbo.cl/uploads/2019/04/JUMBO.STORE_.CL-HOME-DESKTOP.jpg" alt="Third slide">
    </div>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>



<!--OFERTAS-->

<div class="ofertas">
    <h1 style="text-align:center;">Ofertas</h1>
    <a href=""><img src="https://www.jumbo.cl/uploads/2019/04/calugas_desktop_S18-pescados.jpg"></a>
    <a href=""><img src="https://www.jumbo.cl/uploads/2019/04/Desktop-calugas_S18_palta.jpg"></a>
    <a href=""><img src="https://www.jumbo.cl/uploads/2019/04/calugas_desktop_mascotas.jpg">
    </a>
</div>


<!--LLAMANDO A LOS PRODUCTOS DEL COMPONENTE PRODUCTOS.VUE-->
      
<producto></producto>








<!-- MODAL LOGIN Y REGISTRO -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" id="loginModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" name="button" class="close" data-dismiss="modal" aria-label="close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-tittle" id="loginModalLabel"></h4>
        
      </div>


<!-- LO QUE ESTA DENTRO DEL MODAL LOGIN -->
<div class="modal-body">
<div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input data-id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input data-id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>  
</div>      
    </div>
    
  </div>
  </div>
  
<!-- TERMINO DEL MODAL LOGIN-->


<!-- MODAL REGISTRO -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" id="registerModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" name="button" class="close" data-dismiss="modal" aria-label="close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-tittle" id="registerModalLabel"></h4>
        
      </div>


<!-- LO QUE ESTA DENTRO DEL MODAL -->
<div class="modal-body">

 <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" id="form" action="{{ route('register') }}" >
                        @csrf

                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombres') }}</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" value="{{ old('nombre') }}" required autofocus pattern="[a-z]{1,15}">

                                @if ($errors->has('nombre'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="apellido" class="col-md-4 col-form-label text-md-right">{{ __('Apellidos') }}</label>

                            <div class="col-md-6">
                                <input id="apellido" type="text" class="form-control{{ $errors->has('apellido') ? ' is-invalid' : '' }}" name="apellido" value="{{ old('apellido') }}" required autofocus pattern="[a-z]{1,15}">

                                @if ($errors->has('apellido'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('apellido') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="rut" class="col-md-4 col-form-label text-md-right">{{ __('Rut') }}</label>

                            <div class="col-md-6">
                                <input maxlength="12" id="rut"  type="text" class="form-control{{ $errors->has('rut') ? ' is-invalid' : '' }}" name="rut" value="{{ old('rut') }}" required autofocus onkeyup="formato_rut(this)">

                                @if ($errors->has('rut'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('rut') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>

                            <div class="col-md-6">
                                <input id="direccion" type="text" class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion" value="{{ old('direccion') }}" required>

                                @if ($errors->has('direccion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('direccion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="ciudads" class="col-md-4 col-form-label text-md-right">{{ __('Ciudad') }}</label>

                            <div class="col-md-6">
                                 <select id="ciudads" class="form-control @if($errors->has('ciudads')) is-invalid @endif" placeholder=" Ciudad*" name="ciudads" required value="" />
                                 
                                         
                                         @foreach ($ciudads as $ciudad)    
                                            <option value="{{ $ciudad->id }}">{{ $ciudad->name }}</option>
                                        @endforeach

                                </select>
                            </div>
                        </div>


                          

                              <div class="form-group row">
                            <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Telefono') }}</label>

                            <div class="col-md-6">
                                <input id="telefono" type="text" class="form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}" name="telefono" value="{{ old('telefono') }}" required autofocus pattern="[0-9]{9}" title="tienen que ser numeros y 9 digitos">

                                @if ($errors->has('telefono'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        


                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button id="ajaxSubmit" style="margin-left: 60px" type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
  </div>  

</div>      
  </div>
  </div>

 


<!--FOOTER-->

    <footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 myCols">
                    <h5>Get started</h5>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Sign up</a></li>
                        <li><a href="#">Downloads</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 myCols">
                    <h5>About us</h5>
                    <ul>
                        <li><a href="#">Company Information</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Reviews</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 myCols">
                    <h5>Support</h5>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Help desk</a></li>
                        <li><a href="#">Forums</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 myCols">
                    <h5>Legal</h5>
                    <ul>
                        <li><a href="#">Terms of Service</a></li>
                        <li><a href="#">Terms of Use</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="social-networks">
            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="#" class="facebook"><i class="fa fa-facebook-official"></i></a>
            <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
        </div>
        <div class="footer-copyright">
            <p>© 2016 Copyright SuperMarket </p>
        </div>
    </footer>
</div>
<!--TERMINO DEL DIV DEL LLAMDO DE LOS COMPONENTES VUEJS-->


@include('partials.scripts')

@if($errors->has('rut'))
            <script>
             $('#registerModal').modal('show');
            </script>
        @endif 
      

 
    <!--
<script>
         jQuery(document).ready(function(){
            jQuery('#ajaxSubmit').click(function(e){
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
            });
               jQuery.ajax({
                  url: "{{ route('register') }}",
                  method: 'post',
                  data: {
                     nombre: jQuery('#nombre').val(),
                     apellido: jQuery('#apellido').val(),
                     rut: jQuery('#rut').val(),
                     email: jQuery('#email').val(),
                     telefono: jQuery('#telefono').val(),
                     password: jQuery('#password').val(),
                  
                  },
                  success: function(result){
                    if(result.errors)
                    {
                        jQuery('.alert-danger').html('');

                        jQuery.each(result.errors, function(key, value){
                            jQuery('.alert-danger').show('');
                            jQuery('.alert-danger').append('<li>'+value+'</li>');
                        });
                    }
                    else
                    {
                        jQuery('.alert-danger').hide();
                       
                    }
                  }});
               });
            });
      </script>

MODAL LOGIN ALERT 
  -->

</body>
</html>