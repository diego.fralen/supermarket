<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{csrf_token()}}">
@include('partials.headers')
<link rel="stylesheet" type="text/css" href="{{asset('css/admin.css')}}">

<title>Admin</title> 
</head>
<body>
    <div id="app">
        <menu-admin user="{{ auth()->user()->NombreCompleto() }}"></menu-admin>
    </div>

@include('partials.scripts')
</body>
</html>