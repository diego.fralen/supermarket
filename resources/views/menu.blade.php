<html>
<head>
	
@include('partials.headers')
<title>Menu</title>	
</head>
<body>
	<div id="app">
		<menu-main user="{{ auth()->user()->NombreCompleto()}}" rut="{{auth()->user()->rut}}"
		email="{{auth()->user()->email}}"
			telefono="{{auth()->user()->telefono}}" direccion="{{auth()->user()->direccion}}"></menu-main>

	</div>

@include('partials.scripts')
</body>
</html>