<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"><img src="https://s3-us-west-2.amazonaws.com/joinnus.com/user/362305/act5ac7cca3ec6d1.jpg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## SUPERMARKET

En esta aplicación se implementa php laravel con componente vue js:

- Login y Registro de Usuarios.
- Productos.
- Carro Compra(sin metodo de pago).
- Crud en vue js:
- Categorias.
- Productos.
- Proveedores.
- Registro de Ventas
- Clientes


## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Developers
 - Diego Ramos Rios(https://www.facebook.com/dieg0ramos)
 - Joaquin Rojo (https://www.facebook.com/profile.php?id=100009302149725)
 - Luciano Olmos(https://www.facebook.com/QueZaenDeMi)