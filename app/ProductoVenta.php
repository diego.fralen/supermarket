<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoVenta extends Model
{

	  protected $fillable = [
        'fecha_venta','user_id','total'
    ];


     public function user(){
        return $this->BelongsTo('App\User');
    }
    
   public function venta(){
   	return $this->hasMany('App\Venta');
   }

   public function descuento(){
   	return $this->hasMany('App\Descuento');
   }
}
