<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
	protected $table = "proveedores";
	protected $fillable = [
        'nombre','apellido', 'rut','direccion','telefono','email'
    ];

   public function detallefactura(){
   	return $this->hasMany('App\DetalleFactura');
   }
}
