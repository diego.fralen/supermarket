<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetodoPago extends Model
{
     protected $fillable = [
        'nombre','descripcion'
    ];

    public function venta(){
    	return $this->hasMany('App\Venta');
    }
}
