<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleFactura extends Model
{
     protected $fillable = [
        'numero_factura','fecha_compra','precio_venta','cantidad','proveedor_id','producto_id','total'
    ];

    public function producto(){
        return $this->belongsTo('App\Producto');
    }
    public function proveedor(){
    return $this->belongsTo('App\Proveedor');
    }
     
}
