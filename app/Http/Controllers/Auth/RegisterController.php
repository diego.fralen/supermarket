<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Rules\Rut;
use App\Helpers\RutFormatter;
use App\Request\StoreRegister;
Use App\Ciudad;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/menu';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'nombre' => ['required', 'string', 'max:255'],
             'apellido' => ['required', 'string'],
             'rut' => ['required', 'string','unique:users',new Rut],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
             'telefono' => ['required'],
            'password' => ['required', 'string'],
            'direccion' => ['required', 'string']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
       /*  TABLACONTRA:create([
            'password' => Hash::make($data['password'])
        ]);*/
        return User::create([
            'nombre' => $data['nombre'],
            'apellido' => $data['apellido'],
            'rut' => $data['rut'],
            'email' => $data['email'],
             'telefono' => $data['telefono'],
             'password' => Hash::make($data['password']),
             'ciudad_id'=>$data['ciudads'],
             'direccion'=>$data['direccion'],
        ]);
         
         return back()->with(['msg' => 'se pudo registrar']);
    }
}
