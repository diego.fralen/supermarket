<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
  use AuthenticatesUsers;

  protected $redirectTo = '/admin';
  
    public function __construct()
    {
       $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
      return view('auth.admin-login');
    }

 protected function guard()
    {
        return Auth::guard('admin');
    }
  
}
