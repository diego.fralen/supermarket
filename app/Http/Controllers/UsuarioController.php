<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsuarioController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('menu');
    }
   public function checkout()
    {
        return view('checkout');
    }

    public function product()
    {
        $product = Product::all();
        return view('productos', compact('products'));
    }
    public function logout(Request $request) {
    $request->User()->token()->revoke();
    return response()->json([
       'message' => 'Successfully logged out'
    ]);
     return view('welcome');
  }
}
