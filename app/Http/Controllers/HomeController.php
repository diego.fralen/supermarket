<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreRegister;
use App\Producto;
use App\User;
use App\Ciudad;

class HomeController extends Controller
{
   
    public function home()
    {
        return view('welcome');
    }
    public function show(){
          $ciudads = Ciudad::all();
        return view('welcome',compact('ciudads'));
    }
    

      public function index()
    {
        return view('/productos');
    }
}
