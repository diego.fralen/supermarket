<?php

namespace App\Http\Controllers;
use Auth;
use App\Producto;
use App\Categoria;
use App\Proveedor;
use App\Marca;
use App\User;
use App\DetalleFactura;
use Illuminate\Http\Request;
use App\Http\Requests\StoreRegister;
use Carbon\Carbon;
use App\ProductoVenta;
use App\Venta;

class Api extends Controller
{
  //MOSTRAR PRODUCTOS
      public function listas_producto(){
            
    
    $productos=Producto::with('categoria')->with('marca')->get();
   // $productodo=Producto::select('users.id','users.nombre')->rightjoin('producto_ventas', 'productos.id', '=', 'producto_ventas.producto_id')
    //->rightjoin('users','producto_ventas.user_id','=' ,'users.id')->get();
    
    //($datos = array_merge([$productos,$productodo]);

    return response()->json($productos);
   }

//Mostrar Marcas
   public function marcas(){
    $marcas=Marca::get();
    return response()->json($marcas);
   }

// MOPSTRAR CATEGORIAS
   public function listas_categoria(){
   //	$categorias = Categoria::paginate(5);
      $categorias = Categoria::orderBy('nombre')->get();
   	return response()->json($categorias);
   }



   //se crea las Categorias
   public function create(Request $request){
   	$categorias = Categoria::create($request->all());
   	return response()->json(['categorias'=>$categorias]);
   }
  //DELETE CATEGORIAS
   public function destroy($id){
   	
   	 $Categorias = Categoria::findOrFail($id);
   	 $Categorias->delete();
   	return '';
   }


   public function edit(Request $request,$id){
     $categorias = Categoria::findOrFail($id);
      return response()->json($categorias);
   }
//update CATEGORIAAS
    public function update(Request $request,$id){

      $categorias = Categoria::findOrFail($id);
        $categorias->nombre = $request->nombre;
        $categorias->descripcion = $request->descripcion;
        $categorias->save();


   	return response()->json($categorias);
   }


   //MOSTRAR PROVEEDORES

     public function listas_proveedor(){

    $proveedores = Proveedor::get();
 	
    return response()->json($proveedores);
   }

    //se crea los proovedores
   public function createpro(StoreRegister $request){
      
      $proveedores = new Proveedor;
        $proveedores->nombre=$request->nombre;
        $proveedores->apellido=$request->apellido;
        $proveedores->direccion=$request->direccion;
        $proveedores->rut=$request->rut;
        $proveedores->telefono=$request->telefono;
        $proveedores->email=$request->email;
        $proveedores->save();

      return response()->json(['proveedores'=>$proveedores]);
   
   }
   //ELIMINAR PROVEEDORES
    public function deleteprov($id){
    
     $proveedores = Proveedor::findOrFail($id);
     $proveedores->delete();
    return '';
   }



 //se crea las Productos
   public function createproductos(Request $request){
      $productos = new Producto;
        $productos->nombre=$request->nombre;
        $productos->descripcion=$request->descripcion;
        $productos->stock=$request->stock;
        $productos->precio=$request->precio;
        $productos->marca_id=$request->marca_id;
        $productos->imagen=$request->imagen;
        $productos->categoria_id=$request->categoria_id;
        $productos->codigo=$request->codigo;
        
      

        $productos->save();

      return response()->json(['productos'=>$productos]);
   }

   //ELIMINAR PRODUCTOS

   public function deleteprod($id){
     $productos = Producto::findOrFail($id);
     $productos->delete();
    return '';
   }

   //Editar Productos
   public function editprod(Request $request,$id){
     $productos = Producto::findOrFail($id);
      return response()->json($productos);
   }
//ACTUALIZAR Productos
   public function updateprod(Request $request,$id){

      $productos = Producto::findOrFail($id);
        $productos->nombre=$request->nombre;
        $productos->descripcion=$request->descripcion;
        $productos->stock=$request->stock;
        $productos->precio=$request->precio;
        $productos->marca_id=$request->marca_id;
        $productos->imagen=$request->imagen;
        $productos->categoria_id=$request->categoria_id;
        $productos->codigo=$request->codigo;
        $productos->save();


    return response()->json($productos);
   }


   //MUESTRO LOS CLIENTES EN ADMINISTRADOR

   public function showuser(){
      $users = User::with('ciudad')->get();
  
    return response()->json($users);
   }
   //ELIMINO UN CLIENTE 
    public function removecli($id){
      $users = User::findOrFail($id);
     $users->delete();
    return '';
    }

    public function showcompra_prov(){
      $detalles=DetalleFactura::with(['proveedor','producto.categoria','producto.marca'])->get();
      return response()->json($detalles);
    }

      public function compra_prov(Request $request){
  
        $detalles = new DetalleFactura;
        $detalles->numero_factura=$request->numero_factura;
        $detalles->precio_venta=$request->precio_venta;
        $detalles->cantidad=$request->cantidad;
        $detalles->fecha_compra=$request->fecha_compra;
        $detalles->proveedor_id=$request->proveedor_id;
        $detalles->producto_id=$request->producto_id;
        $detalles->total=$request->total;
        $detalles->save();

      return response()->json(['detalles'=>$detalles]);
   }

   //COMPRA PRODUCTOS CLIENTE
   public function compra(Request $request){

        
      $productoventas = new ProductoVenta;
        $productoventas->fecha_venta=$request->fecha_venta;
        $productoventas->user_id=$request->user_id;
        $productoventas->total=$request->total;
        $productoventas->save();
       
        foreach ($request->carrito as $value) {
         $ventas=new Venta;
        $ventas->producto_id=$value['id'];
        $ventas->producto_venta_id=$productoventas->id;
        $ventas->cantidad=$value['cantidad'];
        $ventas->save();  
       }

       
      return response()->json(['productoventas'=>$productoventas]);
   
   }

   public function deletefactura($id){
     $users = DetalleFactura::findOrFail($id);
     $users->delete();
    return '';
   }

   public function detalleventas(){

    $detalleventas=Producto::Select('users.rut','users.nombre','users.telefono','productos.nombre','productos.imagen','productos.codigo','ventas.cantidad','producto_ventas.fecha_venta','productos.precio','ventas.cantidad','producto_ventas.total')
    ->join('ventas', 'productos.id', '=', 'ventas.producto_id')
    ->join('producto_ventas','ventas.producto_venta_id','=','producto_ventas.id')
    ->join('users','producto_ventas.user_id','=','users.id')->get();

     return response()->json($detalleventas);
 
   }
  

}
