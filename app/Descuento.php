<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descuento extends Model
{
     protected $fillable = [
        'nombre','porcentaje','productoventa_id'
    ];

    public function descuentoproducto(){
    	return $this->hasMany('App\DescuentoProducto');
    }

     public function productoventa(){
    	return $this->belongsTo('App\ProductoVenta');
    }
}
