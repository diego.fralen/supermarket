<?php

namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class RutFormatter extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'RutFormatter';
    }
}
