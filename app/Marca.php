<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $fillable = [
        'nombre','codigo'
    ];

    public function producto(){
    	return $this->hasMany('App\Producto');
    }
}
