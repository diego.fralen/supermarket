<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
     protected $fillable = [
        'producto_venta_id','producto_id','cantidad'
    ];


     public function productoventa(){
        return $this->BelongsTo('App\ProductoVenta');
    }
    public function producto(){
    	return $this->BelongsTo('App\Producto');
    }
}
