<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Rut implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->validateRut($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Rut inválido.';
    }

    private function validateRut($value)
    {
        if (strpos($value, "-") == false) {
            $buffer[0] = substr($value, 0, -1);
            $buffer[1] = substr($value, -1);
        } else {
            $buffer = explode("-", trim($value));
        }

        $rut = str_replace(".", "", trim($buffer[0]));

        if (!is_numeric($rut)) {
            return false;
        }

        $factor = 2;
        $sum = 0;
        for ($i = strlen($rut)-1; $i >= 0; $i--) {
            $factor = $factor > 7 ? 2 : $factor;
            $sum += $rut{$i} * $factor++ ;
        }

        $remainder = $sum % 11;
        $dv = 11 - $remainder;

        if ($dv == 11) {
            $dv=0;
        } elseif ($dv == 10) {
            $dv="k";
        }

        return $dv == trim(strtolower($buffer[1])) ? true : false;
    }
}
