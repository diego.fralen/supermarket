<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{

    protected $fillable = [
        'nombre','descripcion','precio','imagen','categoria_id','stock','marca_id','codigo'
    ];

	 protected $table = "productos";
	 
    public function categoria(){
    	return $this->belongsTo('App\Categoria');
    }
    public function detallefactura(){
        return $this->hasMany('App\DetalleFactura');
    }
     public function venta(){
        return $this->hasMany('App\Venta');
    }
     public function descuentoproducto(){
        return $this->hasMany('App\DescuentoProducto');
    }

    public function marca(){
        return $this->belongsTo('App\Marca');
    }
}
