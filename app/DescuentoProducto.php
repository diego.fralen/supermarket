<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descuento_Producto extends Model
{
      protected $fillable = [
        'descuento_id','productos_id','inicio','termino'
    ];

    public function producto(){
    	return $this->belongsTo('App\Producto');
    }
    public function descuento(){
    	return $this->belongsTo('App\Descuento');
    }
}
